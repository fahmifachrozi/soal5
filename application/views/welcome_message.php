<html>
<head>
<title>Soal no 5</title>
<style>
table {
  width:100%;
}
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
th, td {
  padding: 15px;
  text-align: center;
}
table#t01 tr:nth-child(even) {
  background-color: #eee;
}
table#t01 tr:nth-child(odd) {
 background-color: #fff;
}
table#t01 th {
  background-color: black;
  color: white;
}
</style>
</head>
<body>
    <table id="t01">
        <tr>
          <th>ID</th>
          <th>Nama</th> 
          <th>Kelas</th>
          <th>Jurusan</th>
		</tr>
		<?php foreach($data_siswa as $data){ ?>
        <tr>
          <td><?= $data->id; ?></td>
          <td><?= strtoupper($data->nama); ?></td>
          <td><?= strtoupper($data->kelas); ?></td>
          <td><?= strtoupper($data->jurusan); ?></td>
		</tr>
		<?php } ?>
   
      </table>
</body>
</html>