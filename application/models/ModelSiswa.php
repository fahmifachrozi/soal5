<?php
class ModelSiswa extends CI_Model {
	function show_data()
	{
			
            $this->db->select('tb_siswa.id,tb_siswa.nama,tb_kelas.kelas,tb_jurusan.jurusan');
            $this->db->from('tb_siswa');		       
            $this->db->join('tb_kelas', 'tb_siswa.kelas=tb_kelas.id','left');
            $this->db->join('tb_jurusan', 'tb_siswa.jurusan=tb_jurusan.id','left'); 
            return $this->db->get();
		
	}
}